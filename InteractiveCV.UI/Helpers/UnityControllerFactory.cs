﻿

using System;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Microsoft.Practices.Unity;

namespace InteractiveCV.UI.Helpers
{
    public class UnityControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(RequestContext reqContext, Type controllerType)
        {
            Controller controller;
          

            if (!typeof(IController).IsAssignableFrom(controllerType))
            {
                throw new ArgumentException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Type requested is not a controller: {0}", controllerType.Name), "controllerType");
            }

            try
            {
                controller = MvcUnityContainer.Container.Resolve(controllerType) as Controller;

                if (controller == null)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            CultureInfo.InvariantCulture,
                            "Could not resolve controller with type: {0}", controllerType));
                }

                
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    string.Format(
                        CultureInfo.InvariantCulture, "Error resolving controller {0}", controllerType.Name), ex);
            }

            return controller;
        }

    }
}