﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InteractiveCV.UI.Helpers
{
    public interface IApiHelper
    {
        T Get<T>(string relativeUrl);
        T Get<T>(string relativeUrl, int top, int skip);
        T Get<T>(string relativeUrl, int id);
        ApiResponse<U> Post<T, U>(string relativeUrl, T data);
        ApiResponse<U> Put<T, U>(string relativeUrl, T data);
        string Put<T>(string relativeUrl, int id, T data);
        string Delete(string relativeUrl, int id);
        void Dispose();
        void SetQueryString(string queryString);
        void SetEndPoint(string endpoint);
    }
}