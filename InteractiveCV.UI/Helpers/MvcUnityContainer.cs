﻿

using Microsoft.Practices.Unity;

namespace InteractiveCV.UI.Helpers
{
    public static class MvcUnityContainer
    {
        public static IUnityContainer Container
        {
            get;
            set;
        }
    }
}