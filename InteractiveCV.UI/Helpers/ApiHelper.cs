﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using InteractiveCV.UI.Properties;
using Newtonsoft.Json;


namespace InteractiveCV.UI.Helpers
{
    public class ApiHelper : IDisposable, IApiHelper
    {
        protected string Endpoint;
        protected string QueryString;

        public ApiHelper()
        {
            Endpoint = Settings.Default.PrivateApi_BaseRpcEndPoint;
        }

        public void SetQueryString(string queryString)
        {
            if (!string.IsNullOrEmpty(queryString))
            {
                QueryString = string.Format("?{0}", queryString);
            }
        }

        public void SetEndPoint(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                Endpoint = endpoint;
            }
        }

        public T Get<T>(string relativeUrl)
        {
            return Get<T>(relativeUrl, 0, 0);
        }

        public T Get<T>(string relativeUrl, int top, int skip)
        {
            using (HttpClient httpClient = NewHttpClient())
            {
                string absoluteUrl = GetAbsoluteUrl(relativeUrl);
                List<string> parameters = new List<string>();

                if (top > 0)
                {
                    parameters.Add(string.Concat("$top=", top));
                }

                if (skip > 0)
                {
                    parameters.Add(string.Concat("$skip=", skip));
                }

                if (parameters.Count > 0)
                {
                    absoluteUrl += (absoluteUrl.Contains("?") ? "&" : "?") + string.Join("&", parameters);
                }

                HttpResponseMessage response = httpClient.GetAsync(absoluteUrl).Result;
                string contentString = response.Content.ReadAsStringAsync().Result;
                return EnsureSuccessStatusCodeAndDeserialiseResponse<T>(response, contentString);
            }
        }

        public T Get<T>(string relativeUrl, int id)
        {
            using (HttpClient httpClient = NewHttpClient())
            {
                string absoluteUrl = GetAbsoluteUrl(string.Format("{0}/{1}", relativeUrl, id));

                HttpResponseMessage response = httpClient.GetAsync(absoluteUrl).Result;
                string contentString = response.Content.ReadAsStringAsync().Result;
                return EnsureSuccessStatusCodeAndDeserialiseResponse<T>(response, contentString);
            }
        }

        public ApiResponse<U> Post<T, U>(string relativeUrl, T data)
        {
            using (HttpClient httpClient = NewHttpClient())
            {
                string absoluteUrl = GetAbsoluteUrl(relativeUrl);
                HttpResponseMessage result = httpClient.PostAsJsonAsync(absoluteUrl, data).Result;
                string contentString = result.Content.ReadAsStringAsync().Result;

                HttpStatusCode httpStatusCode = result.StatusCode;
                try
                {
                    HttpResponseMessage response = result.EnsureSuccessStatusCode();
                    U content = result.Content.ReadAsAsync<U>().Result;
                    return new ApiResponse<U>
                    {
                        Data = content
                    };
                }
                catch (HttpRequestException ex)
                {
                    HttpRequestException requestException = ConstructHttpRequestionException(contentString, ex, httpStatusCode);
                    throw requestException;
                }
            }
        }

        public ApiResponse<U> Put<T, U>(string relativeUrl, T data)
        {
            using (HttpClient httpClient = NewHttpClient())
            {
                string absoluteUrl = GetAbsoluteUrl(relativeUrl);
                HttpResponseMessage result = httpClient.PutAsJsonAsync(absoluteUrl, data).Result;
                string contentString = result.Content.ReadAsStringAsync().Result;

                HttpStatusCode httpStatusCode = HttpStatusCode.OK;
                try
                {
                    HttpResponseMessage response = result.EnsureSuccessStatusCode();
                    httpStatusCode = response.StatusCode;
                    U content = result.Content.ReadAsAsync<U>().Result;
                    return new ApiResponse<U>
                    {
                        Data = content
                    };
                }
                catch (HttpRequestException ex)
                {
                    HttpRequestException requestException = ConstructHttpRequestionException(contentString, ex, httpStatusCode);
                    throw requestException;
                }
            }
        }

        public string Put<T>(string relativeUrl, int id, T data)
        {
            using (HttpClient httpClient = NewHttpClient())
            {
                string absoluteUrl = GetAbsoluteUrl(string.Format("{0}/{1}", relativeUrl, id));
                HttpResponseMessage response = httpClient.PutAsJsonAsync(absoluteUrl, data).Result;

                response.EnsureSuccessStatusCode();
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        public string Delete(string relativeUrl, int id)
        {
            using (HttpClient httpClient = NewHttpClient())
            {
                string absoluteUrl = GetAbsoluteUrl(string.Format("{0}/{1}", relativeUrl, id));
                HttpResponseMessage response = httpClient.DeleteAsync(absoluteUrl).Result;

                response.EnsureSuccessStatusCode();
                return response.Content.ToString();
            }
        }

        public void Dispose()
        {
            Endpoint = null;
        }

        private static T EnsureSuccessStatusCodeAndDeserialiseResponse<T>(HttpResponseMessage response, string contentString)
        {
            try
            {
                response.EnsureSuccessStatusCode();
                return JsonConvert.DeserializeObject<T>(contentString);
            }
            catch (HttpRequestException ex)
            {
                HttpRequestException requestException = ConstructHttpRequestionException(contentString, ex, response.StatusCode);
                throw requestException;
            }
        }

        private static HttpRequestException ConstructHttpRequestionException(string contentString, HttpRequestException httpRequestException, HttpStatusCode httpStatusCode)
        {
            string errorMessage = GetErrorMessage(contentString);
            return new HttpRequestException(string.Format("Status Code [{0}] : {1}", httpStatusCode, errorMessage), httpRequestException);
        }

        private static dynamic GetErrorMessage(string contentString)
        {
            try
            {
                dynamic error = JsonConvert.DeserializeObject<dynamic>(contentString);
                return error.Message;
            }
            catch
            {
                return contentString;
            }
        }

        private string GetAbsoluteUrl(string relativeUrl)
        {
            return string.Format("{0}{1}{2}", Endpoint, relativeUrl, QueryString);
        }

        protected HttpClient NewHttpClient()
        {
            HttpClient httpClient = new HttpClient(new HttpClientHandler
            {
                UseDefaultCredentials = true,
                AutomaticDecompression = DecompressionMethods.GZip
            });
            httpClient.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
            return httpClient;
        }
    }
}