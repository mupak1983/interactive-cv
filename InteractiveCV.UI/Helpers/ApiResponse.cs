﻿

namespace InteractiveCV.UI.Helpers
{
    public class ApiResponse<T>
    {
        public T Data
        {
            get;
            set;
        }

        public string CreatedUrl
        {
            get;
            set;
        }
    }
}