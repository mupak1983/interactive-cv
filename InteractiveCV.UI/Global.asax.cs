﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using InteractiveCV.UI.Helpers;
using Microsoft.Practices.Unity;

namespace InteractiveCV.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RegisterTypes();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private static void RegisterTypes()
        {
            UnityContainer container = new UnityContainer();
            container.RegisterType<IApiHelper, ApiHelper>();
         

            MvcUnityContainer.Container = container;

            // Override the standard controller factory to allow for constructor injection
            // from Unity.
            ControllerBuilder.Current.SetControllerFactory(typeof(UnityControllerFactory));
        }
    }
}
