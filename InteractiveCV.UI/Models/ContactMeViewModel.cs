﻿

using System.ComponentModel.DataAnnotations;

namespace InteractiveCV.UI.Models
{
    public class ContactMeViewModel
    {
        [Required(ErrorMessage="Name is Required")]
        [MinLength(5, ErrorMessage = "Name must be at least 5 characters")]
        [MaxLength(150, ErrorMessage = "Name must not be more than 150 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email is Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        public string Comments { get; set; }
    }
}