﻿"use strict";

angular.module("cvApp").controller("certificationDetailsController", ['$scope', function ($scope) {
    var vm = this;
    vm.certification = $scope.certification;
}]);