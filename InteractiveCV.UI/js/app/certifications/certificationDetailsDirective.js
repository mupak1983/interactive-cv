﻿"use strict";

angular.module("cvApp").directive("certificationDetails", function () {
    return {
        scope: {
            certification: "=" 
        },
        templateUrl: "/js/app/certifications/certificationDetailsTemplate.html",
        controller: "certificationDetailsController",
        controllerAs: 'vm'
    }
});