﻿"use strict";

angular.module("cvApp").factory("certificationService", ['$http', 'certificationsConfig', function ($http, certificationsConfig) {

    var certifications = [];

    loadCertifications();

    function loadCertifications() {
        $http.post(certificationsConfig.dataUrl)
            .success(function (data) {
                certifications.addRange(data);
            });
    }

    var svc = {
        certifications: certifications
    };

    return svc;

}]);