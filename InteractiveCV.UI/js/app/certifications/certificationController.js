﻿"use strict";

angular.module("cvApp").controller("certificationController", ['certificationService', function (certificationService) {
    var vm = this;
    vm.certifications = certificationService.certifications;
}]);