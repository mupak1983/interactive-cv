﻿"use strict";
angular.module("cvApp").controller("experienceController", ['experienceService', function (experienceService) {
    var vm = this;
    vm.experiences = experienceService.experiences;
    vm.error = experienceService.getError();
}])