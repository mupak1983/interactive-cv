﻿"use strict";

angular.module("cvApp").directive("experienceDetails", function() {
    return {
        scope: {
            experience: '='
        },
        templateUrl: "/js/app/experience/experienceDetailsTemplate.html",
        controller: "experienceDetailsController",
        controllerAs: 'vm'
    }
});
