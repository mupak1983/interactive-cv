﻿"use strict";
angular.module("cvApp").factory("experienceService", ['$http', 'experienceConfig', function experienceService($http, experienceConfig) {
    var experiences = [];
    var error;

    loadExperiences();

    function loadExperiences() {
        $http.post(experienceConfig.dataUrl)
            .success(function (data) {
                experiences.addRange(data);
            }).error(function (er) {
                error = "Experiences could not be retrieved due to an error: " + er;
            });
    }

    function getError() {
        return error;
    }

    var svc = {
        experiences: experiences,
        getError: getError
    };

    return svc;

}]);