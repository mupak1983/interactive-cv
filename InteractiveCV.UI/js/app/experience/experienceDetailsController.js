﻿"use strict";

angular.module("cvMenu").controller("experienceDetailsController", ['$scope', function ($scope) {
    var vm = this;
    vm.experience = $scope.experience;
}]);