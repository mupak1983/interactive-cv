﻿"use strict";

angular.module("cvApp").service("infrastructureService", ['$http',function ($http) {


    function log(msg) {
        console.log(msg);
    };


    var svc = {
        log: log
    };

    return svc;

}]);