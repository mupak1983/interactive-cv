﻿"use strict";

angular.module("cvMenu").directive("cvMenu", function() {
    return {
        transclude:true,
        scope: {
            
        },
        replace:true,
        templateUrl: "/js/app/modules/cvMenu/cvMenuTemplate.html",
        controller:"cvMenuController"
    }
});