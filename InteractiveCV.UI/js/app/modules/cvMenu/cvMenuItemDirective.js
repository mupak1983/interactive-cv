﻿"use strict";

angular.module("cvMenu").directive("cvMenuItem", function() {
    return {
        transclude: false,
        scope: {
            title: "@",
            url: "@",
            styles:"@"
        },
        replace:true,
        controller: "cvMenuItemController",
        templateUrl: "/js/app/modules/cvMenu/cvMenuItemTemplate.html"
    }
});