﻿"use strict";

angular.module("cvApp").directive("skillDetails", function () {
    return {
        scope: {
            skill: "=" 
        },
        templateUrl: "/js/app/skills/skillDetailsTemplate.html",
        controller: "skillDetailsController",
        controllerAs: 'vm'
    }
});