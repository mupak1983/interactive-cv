﻿"use strict";

angular.module("cvApp").factory("skillsService", ['$http', 'skillsConfig', function ($http, skillsConfig) {

    var skills = [];

    loadSkills();

    function loadSkills() {
        $http.post(skillsConfig.dataUrl)
            .success(function (data) {
                skills.addRange(data);
            });
    }

    var svc = {
        skills: skills
    };

    return svc;

}]);