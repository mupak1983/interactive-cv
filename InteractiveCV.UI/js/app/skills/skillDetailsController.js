﻿"use strict";

angular.module("cvApp").controller("skillDetailsController", ['$scope', function ($scope) {
    var vm = this;
    vm.skill = $scope.skill;
}]);