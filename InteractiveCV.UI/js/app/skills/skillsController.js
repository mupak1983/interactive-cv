﻿"use strict";

angular.module("cvApp").controller("skillsController", ['skillsService', function (skillsService) {
    var vm = this;
    vm.skills = skillsService.skills;
}]);