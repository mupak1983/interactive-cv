﻿"use strict";

window.app.controller("contactMeController", ['contactMeConfig', 'contactMeService', function (contactMeConfig, contactMeService) {
    var vm = this;
    vm.save = function () {
        contactMeService.save(contactMeConfig.saveUrl, vm.contact).then(function(data) {
            if (!data.error) {
                vm.success = data.success;
                vm.error = null;
            } else {
                vm.error = data.error;
                vm.success = null;
            }
        });
        
    }
}]);