﻿"use strict";

angular.module("cvApp").factory("contactMeService", ['$http', 'infrastructureService', function ($http, infrastructureService) {

    function save(url, contact) {


        var promise = $http.post(url, contact).then(function(response) {
            infrastructureService.log('success is : ' + response.data.success);
            infrastructureService.log('error is : ' + response.data.error);
            return response.data;
        });
        return promise;
        
    };
  
    var svc = {
        save: save
    };

    return svc;

}]);