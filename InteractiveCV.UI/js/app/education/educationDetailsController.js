﻿"use strict";

angular.module("cvApp").controller("educationDetailsController", ['$scope', function ($scope) {
    var vm = this;
    vm.education = $scope.education;
}]);