﻿"use strict";

angular.module("cvApp").directive("educationDetails", function () {
    return {
        scope: {
            education: "=" 
        },
        templateUrl: "/js/app/education/educationDetailsTemplate.html",
        controller: "educationDetailsController",
        controllerAs: 'vm'
    }
});