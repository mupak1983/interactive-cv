﻿"use strict";

angular.module("cvApp").factory("educationService", ['$http', 'educationConfig', function ($http, educationConfig) {

    var education = [];

    loadEducation();

    function loadEducation() {
        $http.post(educationConfig.dataUrl)
            .success(function (data) {
                education.addRange(data);
            });
    }

    var svc = {
        education: education
    };

    return svc;

}]);