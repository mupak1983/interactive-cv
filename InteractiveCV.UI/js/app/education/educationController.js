﻿"use strict";

angular.module("cvApp").controller("educationController", ['educationService', function (educationService) {
    var vm = this;
    vm.education = educationService.education;
}]);