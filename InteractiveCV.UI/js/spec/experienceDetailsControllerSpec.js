﻿"use strict";

describe("experienceDetailsController", function () {

    var $controllerConstructor;
    var scope;
    
    

    beforeEach(module('cvApp'));

    beforeEach(inject(function ($controller, $rootScope) {
        
        $controllerConstructor = $controller;
        scope = $rootScope.$new();
    }));


    it('should have an experience object', function () {
        scope.experience = { "companyName": "We Buy Any Car Ltd", "address": "Manchester, United Kingdom", "logoUrl": "http://abh.com", "tenure": "April 2013 to Present", "keyResponsibilities": ["resp 1", "resp 2"], "projects": [{ "name": "WBAC Finance", "descrption": "desc", "role": "Senior Software Engineer", "toolsAndTechnologies": ["ASP.NET 4.5", "C#.NET"] }] };
        var ctrl = $controllerConstructor('experienceDetailsController', { $scope: scope });
        expect(ctrl.experience).not.toBe(null);
        expect(ctrl.experience.companyName).toBe("We Buy Any Car Ltd");
    });

   
});