﻿"use strict";

describe("experienceService",function() {

    var httpBackend, experienceSvc, experienceCfg;

    beforeEach(module('cvApp',function($provide) {
        $provide.constant('experienceConfig', { dataUrl: "/data/experiences" });
    }));

    beforeEach(inject(function ($httpBackend,  experienceService) {
        httpBackend = $httpBackend;
        experienceSvc = experienceService;
        
    }));



    it("should return 2 experiences", function () {
        httpBackend.expectPOST('/data/experiences').respond([{ "companyName": "We Buy Any Car Ltd", "address": "Manchester, United Kingdom", "logoUrl": "http://abh.com", "tenure": "April 2013 to Present", "keyResponsibilities": ["resp 1", "resp 2"], "projects": [{ "name": "WBAC Finance", "descrption": "desc", "role": "Senior Software Engineer", "toolsAndTechnologies": ["ASP.NET 4.5", "C#.NET"] }] }, { "companyName": "Promethean Limited ", "address": "Blackburn, United Kingdom", "logoUrl": "http://abh.com", "tenure": "Mar 2012 to April 2013 ", "keyResponsibilities": [], "projects": [{ "name": "1. Promethean Planet ", "descrption": "It’s a", "role": null, "toolsAndTechnologies": [] }, { "name": "Planet For Schools ", "descrption": "It is a customized version", "role": "Web Developer", "toolsAndTechnologies": ["ASP.NET 3.5, 4.0"] }] }]);
        httpBackend.flush();
        expect(experienceSvc.experiences.length).toBe(2);
    });


    it("should return 0 experiences and an error", function () {
        httpBackend.expectPOST('/data/experiences').respond(400, 'test error');
        httpBackend.flush();
        expect(experienceSvc.experiences.length).toBe(0);
        expect(experienceSvc.getError()).toBe('Experiences could not be retrieved due to an error: test error');
    });
});