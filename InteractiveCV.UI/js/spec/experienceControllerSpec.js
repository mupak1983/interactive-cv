﻿"use strict";

describe("experienceController", function () {

    var $controllerConstructor;
    var scope;
    var experienceSvc;
    

    beforeEach(module('cvApp', function ($provide) {
        $provide.constant('experienceConfig', { dataUrl: "/data/experiences" });
    }));

    beforeEach(inject(function ($controller, $rootScope, experienceService) {
        
        $controllerConstructor = $controller;
        experienceSvc = experienceService;
        scope = $rootScope.$new();
    }));


    it('should have 2 experiences and null error', function () {
        experienceSvc.experiences = [{ "companyName": "We Buy Any Car Ltd", "address": "Manchester, United Kingdom", "logoUrl": "http://abh.com", "tenure": "April 2013 to Present", "keyResponsibilities": ["resp 1", "resp 2"], "projects": [{ "name": "WBAC Finance", "descrption": "desc", "role": "Senior Software Engineer", "toolsAndTechnologies": ["ASP.NET 4.5", "C#.NET"] }] }, { "companyName": "Promethean Limited ", "address": "Blackburn, United Kingdom", "logoUrl": "http://abh.com", "tenure": "Mar 2012 to April 2013 ", "keyResponsibilities": [], "projects": [{ "name": "1. Promethean Planet ", "descrption": "It’s a", "role": null, "toolsAndTechnologies": [] }, { "name": "Planet For Schools ", "descrption": "It is a customized version", "role": "Web Developer", "toolsAndTechnologies": ["ASP.NET 3.5, 4.0"] }] }];
        spyOn(experienceSvc, "getError").and.returnValue(null);
        var ctrl = $controllerConstructor('experienceController', { $scope: scope, experienceService: experienceSvc });
        expect(ctrl.experiences.length).toBe(2);
        expect(ctrl.error).toBe(null);
    });

    it('should have 0 experiences and an error', function () {
        experienceSvc.experiences = [];
        spyOn(experienceSvc, "getError").and.returnValue("test error");
        var ctrl = $controllerConstructor('experienceController', { $scope: scope, experienceService: experienceSvc });
        expect(ctrl.experiences.length).toBe(0);
        expect(ctrl.error).toBe("test error");
    });
});