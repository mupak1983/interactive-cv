﻿"use strict";

describe("experienceDetailsDirective", function () {
    var element, scope, $compile;

    beforeEach(module('cvApp')); // Name of the module my directive is in
    beforeEach(module('/js/app/experience/experienceDetailsTemplate.html')); // The external template file referenced by templateUrl

    beforeEach(inject(function (_$compile_, $rootScope) {
        scope = $rootScope;
        $compile = _$compile_;
    }));

    it("empty address", function () {
        scope.experience = { "companyName": "We Buy Any Car Ltd", "address": "Manchester, United Kingdom", "logoUrl": "http://abh.com", "tenure": "April 2013 to Present", "keyResponsibilities": ["resp 1", "resp 2"], "projects": [{ "name": "WBAC Finance", "descrption": "desc", "role": "Senior Software Engineer", "toolsAndTechnologies": ["ASP.NET 4.5", "C#.NET"] }] };

        // Create an instance of the directive
        element = angular.element('<experience-details experience="experience"></experience-details>');
        $compile(element)(scope); // Compile the directive
        scope.$digest(); // Update the HTML

        // Get the isolate scope for the directive
        var isoScope = element.isolateScope();

        // Make our assertions
        
    });
});