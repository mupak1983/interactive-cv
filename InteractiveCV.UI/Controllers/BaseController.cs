﻿

using System.Web.Mvc;
using InteractiveCV.UI.ActionResults;

namespace InteractiveCV.UI.Controllers
{
    public class BaseController : Controller
    {
        public BetterJsonResult<T> BetterJson<T>(T model)
        {
            return new BetterJsonResult<T>() { Data = model };
        }
    }
}