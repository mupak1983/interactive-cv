﻿using System.Collections.Generic;
using System.Web.Mvc;
using InteractiveCV.Dtos;
using InteractiveCV.UI.Helpers;
using InteractiveCV.UI.Models;

namespace InteractiveCV.UI.Controllers
{
    public class CvController : BaseController
    {
        public ActionResult Info()
        {
            return View();
        }

        public ActionResult Experience()
        {
            return View();
        }

        public ActionResult Skills()
        {
            return View();
        }

        public ActionResult Education()
        {
            return View();
        }

        public ActionResult Certifications()
        {
            return View();
        }

        public ActionResult ContactMe()
        {
            return View(new ContactMeViewModel());
        }
    }
}