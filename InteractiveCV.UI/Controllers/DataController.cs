﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using InteractiveCV.Dtos;
using InteractiveCV.UI.Helpers;
using InteractiveCV.UI.Models;
using Newtonsoft.Json.Serialization;

namespace InteractiveCV.UI.Controllers
{
    public class DataController : BaseController
    {
         private IApiHelper _apiHelper;
         public DataController(IApiHelper apiHelper)
         {
             _apiHelper = apiHelper;
         }


         public JsonResult Experiences()
         {
             //TODO: handle any exception and return error
             var experiences = _apiHelper.Get<List<Experience>>("experience/getall");
             return BetterJson(experiences.ToArray());
         }

         public JsonResult Skills()
         {
             //TODO: handle any exception and return error
             var skills = _apiHelper.Get<List<Skill>>("skill/getall");
             return BetterJson(skills.ToArray());
         }

         public JsonResult Education()
         {
             //TODO: handle any exception and return error
             var education = _apiHelper.Get<List<Education>>("education/getall");
             return BetterJson(education.ToArray());
         }

         public JsonResult Certifications()
         {
             //TODO: handle any exception and return error
             var certifications = _apiHelper.Get<List<Certification>>("certification/getall");
             return BetterJson(certifications.ToArray());
         }

        [HttpPost]
         public JsonResult ContactMe(ContactMeViewModel model)
        {
            if (model == null)
            {
                return BetterJson(new{Error= "Null contact me model supplied."});
            }

            //TODO: post to web api
            //TODO: handle any exception and return error
            if (!ModelState.IsValid)
            {
                return BetterJson(
                    new
                    {
                        Error = string.Join("<br/>", ModelState.Values
                            .SelectMany(x => x.Errors)
                            .Select(x => x.ErrorMessage))
                    });

            }
            return BetterJson(new
            {
                Success=string.Format("Thanks {0} for contacting me. I shall get in touch with you at {1} as soon as possible.",model.Name, model.Email)
            });
        }
    }
}