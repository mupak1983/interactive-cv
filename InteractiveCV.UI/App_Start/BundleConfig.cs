﻿
using System.Web.Optimization;

namespace InteractiveCV.UI
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/css/all.css")
                .Include("~/Content/css/font-awesome.css")
                .Include("~/Content/css/bootstrap.css")
                .Include("~/Content/css/site.css")
                //.Include("~/css/ui-grid.css")
                );

            bundles.Add(new ScriptBundle("~/js/all.js")
                .Include("~/js/lib/jquery-1.9.0.js")
                .Include("~/js/lib/bootstrap.js")
                .Include("~/js/lib/angular.js")
                .Include("~/js/app/helpers/arrayExtensions.js")
            //    .Include("~/js/angular.js")
            //    .Include("~/js/angular-animate.js")
            //    .Include("~/js/ui-bootstrap.js")
            //    .Include("~/js/ui-grid.js")
            //    .Include("~/js/app/app.js")
            //    .IncludeDirectory("~/js/app/", "*.js", true)
                );
        }
    }
}