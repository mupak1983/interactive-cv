﻿

namespace InteractiveCV.Dtos
{
    public class Certification
    {
        public int Year { get; set; }
        public string Description { get; set; }
        public string LogoUrl { get; set; }
    }
}
