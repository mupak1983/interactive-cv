﻿

namespace InteractiveCV.Dtos
{
    public class Skill
    {
        public string Name { get; set; }
        public int Level { get; set; }
    }
}
