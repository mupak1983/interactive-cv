﻿

namespace InteractiveCV.Dtos
{
    public class Education
    {
        public int Year { get; set; }
        public string Description { get; set; }
    }
}
