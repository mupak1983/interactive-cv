﻿

using System.Collections.Generic;

namespace InteractiveCV.Dtos
{
    public class Experience
    {
        public Experience()
        {
            KeyResponsibilities = new List<string>();
        }


        public string CompanyName { get;  set; }
        public string Address { get;  set; }
        public string LogoUrl { get;  set; }
        public string Tenure { get;  set; }
        public List<string> KeyResponsibilities { get;  set; }
        public List<Project> Projects { get;  set; }


        public class Project
        {
            public Project()
            {
                ToolsAndTechnologies = new List<string>();
            }

            public string Name { get; set; }
            public string Descrption { get; set; }
            public string Role { get; set; }
            public List<string> ToolsAndTechnologies { get; set; }
        }

    }
}