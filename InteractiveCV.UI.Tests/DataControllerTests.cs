﻿using System.Collections.Generic;
using InteractiveCV.Dtos;
using InteractiveCV.UI.Controllers;
using InteractiveCV.UI.Helpers;
using InteractiveCV.UI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InteractiveCV.UI.Tests
{
    [TestClass]
    public class DataControllerTests
    {
        private DataController _controller;
        private Mock<IApiHelper> _apiHelper;

        [TestInitialize]
        public void Initialise()
        {
            _apiHelper = new Mock<IApiHelper>();
          
            _controller = new DataController(_apiHelper.Object);
        }

        [TestMethod]
        public void WhenExperiencesAreRetrieved_ThenTheyAreReturnedCorrectly()
        {
            //Arrange
            var experiences = new List<Experience>()
            {
                new Experience()
                {
                    Tenure = "tenure",
                    Address = "manchester",
                    CompanyName = "we buy any car",
                    KeyResponsibilities = new List<string>()
                    {
                        "res 1",
                        "res 2"
                    },
                    LogoUrl = "test logo",
                    Projects = new List<Experience.Project>()
                    {
                        new Experience.Project()
                        {
                            Descrption = "prj 1",
                            Name = "prj 1",
                            Role = "SE"
                        }
                    }
                }
            };
            _apiHelper.Setup(x => x.Get<List<Experience>>("experience/getall")).Returns(experiences);

            //Act
            var actual = _controller.Experiences();

            //Assert
            Assert.IsNotNull(actual);
            var lst = (Experience[])actual.Data;
          Assert.IsNotNull(lst);
            
            Assert.AreEqual(1,lst.Length);
            //TODO: assert each property in object
        }


        [TestMethod]
        public void WhenSkillsAreRetrieved_ThenTheyAreReturnedCorrectly()
        {
            //Arrange
            var arrangeLst = new List<Skill>()
            {
                new Skill()
                {
                   Level = 1,
                   Name = "skill 1"
                },
                new Skill()
                {
                   Level = 2,
                   Name = "skill 2"
                }
            };
            _apiHelper.Setup(x => x.Get<List<Skill>>("skill/getall")).Returns(arrangeLst);

            //Act
            var actual = _controller.Skills();

            //Assert
            Assert.IsNotNull(actual);
            var lst = (Skill[])actual.Data;
            Assert.IsNotNull(lst);

            Assert.AreEqual(2, lst.Length);
            //TODO: assert each property in object
        }


        [TestMethod]
        public void WhenEducationAreRetrieved_ThenTheyAreReturnedCorrectly()
        {
            //Arrange
            var arrangeLst = new List<Education>()
            {
                new Education()
                {
                  Description = "edu 1",
                  Year = 2005
                },
                new Education()
                {
                  Description = "edu 2",
                  Year = 2007
                }
            };
            _apiHelper.Setup(x => x.Get<List<Education>>("education/getall")).Returns(arrangeLst);

            //Act
            var actual = _controller.Education();

            //Assert
            Assert.IsNotNull(actual);
            var lst = (Education[])actual.Data;
            Assert.IsNotNull(lst);

            Assert.AreEqual(2, lst.Length);
            //TODO: assert each property in object
        }


        [TestMethod]
        public void WhenCertificationsAreRetrieved_ThenTheyAreReturnedCorrectly()
        {
            //Arrange
            var arrangeLst = new List<Certification>()
            {
                new Certification()
                {
                  Description = "edu 1",
                  Year = 2005,
                  LogoUrl = "sddd"
                },
                new Certification()
                {
                  Description = "edu 2",
                  Year = 2007
                }
            };
            _apiHelper.Setup(x => x.Get<List<Certification>>("certification/getall")).Returns(arrangeLst);

            //Act
            var actual = _controller.Certifications();

            //Assert
            Assert.IsNotNull(actual);
            var lst = (Certification[])actual.Data;
            Assert.IsNotNull(lst);

            Assert.AreEqual(2, lst.Length);
            //TODO: assert each property in object
        }

        [TestMethod]
        public void WhenNullModelIsSuppliedToContactMe_ThenAnErrorMessageIsReturned()
        {
            
            //Arrange

            //Act
            var jsonResult = _controller.ContactMe(null);
            
            //Assert
            Assert.IsNotNull(jsonResult);
            Assert.IsNotNull(jsonResult.Data);
            dynamic model = jsonResult.Data;
            Assert.AreEqual("Null contact me model supplied.", model.GetType().GetProperty("Error").GetValue(model, null));
            Assert.AreEqual(null, model.GetType().GetProperty("Success"));
        }


        [TestMethod]
        public void WhenAValidRequestIsSuppliedToContactMe_ThenSuccessMessageIsReturned()
        {

            //Arrange
            
            //Act
            var jsonResult = _controller.ContactMe(new ContactMeViewModel(){Name = "usman",Email = "usman@cs.com"});

            //Assert
            Assert.IsNotNull(jsonResult);
            Assert.IsNotNull(jsonResult.Data);
            dynamic model = jsonResult.Data;
            Assert.AreEqual("Thanks usman for contacting me. I shall get in touch with you at usman@cs.com as soon as possible.", model.GetType().GetProperty("Success").GetValue(model, null));
            Assert.AreEqual(null, model.GetType().GetProperty("Error"));
        }



        [TestMethod]
        public void WhenInvalidModelIsSuppliedToContactMe_ThenAnErrorMessageIsReturned()
        {

            //Arrange
            _controller.ModelState.AddModelError("Name", "Error");
            //Act
            var jsonResult = _controller.ContactMe(new ContactMeViewModel() {  Email = "usman@cs.com" });

            //Assert
            Assert.IsNotNull(jsonResult);
            Assert.IsNotNull(jsonResult.Data);
            dynamic model = jsonResult.Data;
            Assert.AreEqual("Error", model.GetType().GetProperty("Error").GetValue(model, null));
            Assert.AreEqual(null, model.GetType().GetProperty("Success"));
        }

    
    }
}
