﻿

using System.IO;
using System.Xml.Serialization;

namespace InteractiveCV.Framework.Xml
{
    public static class XmlSerialisationExtensions
    {
        public static string Serialise<T>(this T toSerialise)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, toSerialise);
                return writer.ToString();
            }
        }

        public static T Deserialise<T>(this string serialisedString) where T : class
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(serialisedString))
            {
                return serializer.Deserialize(reader) as T;
            }
        }
    }
}
