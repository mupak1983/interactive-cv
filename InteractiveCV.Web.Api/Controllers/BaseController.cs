﻿

using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace InteractiveCV.Web.Api.Controllers
{
    public class BaseController : ApiController
    {
        protected HttpResponseMessage BadRequestResponse(Exception ex)
        {
            //log here to elmah
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
        }

        protected HttpResponseMessage BadRequestResponse(string exceptionMessage)
        {
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, exceptionMessage);
        }

        protected HttpResponseMessage OkResponse(string successMessage)
        {
            return Request.CreateResponse(HttpStatusCode.OK, successMessage);
        }

        protected HttpResponseMessage OkResponse<T>(T obj)
        {
            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }

        protected HttpResponseMessage NotFoundResponse(string notFoundMessage)
        {
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, notFoundMessage);
        }
    }
}