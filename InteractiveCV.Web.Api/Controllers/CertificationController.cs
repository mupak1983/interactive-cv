﻿
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using InteractiveCV.Dtos;
using InteractiveCV.Framework.Xml;

namespace InteractiveCV.Web.Api.Controllers
{
    public class CertificationController : BaseController
    {
        [ActionName("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            var fullPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/Certifications.xml");
            var xml = File.ReadAllText(fullPath);
            var certifications = xml.Deserialise<List<Certification>>();
            return OkResponse(certifications);
        }
    }
}