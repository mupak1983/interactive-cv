﻿
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using InteractiveCV.Dtos;
using InteractiveCV.Framework.Xml;

namespace InteractiveCV.Web.Api.Controllers
{
    public class ExperienceController : BaseController
    {
        [ActionName("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            //var experiences = new List<Experience>()
            //{
            //    new Experience()
            //    {
            //        Address = "Manchester",
            //        CompanyName = "abc",
                    
            //        KeyResponsibilities = new List<string>()
            //        {
            //            "one",
            //            "two"
            //        },
            //        LogoUrl = "http://abh.com",
            //        Projects = new List<Experience.Project>()
            //        {
            //            new Experience.Project()
            //            {
            //                Descrption = "dkf",
            //                Name = "dsfsdf",
            //                Role = "sdsd",
            //                ToolsAndTechnologies = new List<string>()
            //                {
            //                    "asp.net",
            //                    "sql"
            //                }
            //            }
            //        },
            //        Tenure = "d"

            //    }
            //};

            var fullPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/Experiences.xml");
            var xml = File.ReadAllText(fullPath);
            var experiences = xml.Deserialise<List<Experience>>();
            return OkResponse(experiences);
        }
    }
}