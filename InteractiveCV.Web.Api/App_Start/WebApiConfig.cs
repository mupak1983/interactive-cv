﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace InteractiveCV.Web.Api
{
    public static class WebApiConfig
    {
        private const string DefaultApiRouteName = "DefaultApi";
        private const string RpcApiRouteName = "RpcApi";

        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            //set up message handlers
            RegisterHandlers(config);

            //set up routes
            CreateRoutes(config);

            //set up Automapper mappings
            RegisterMappings();
        }

        private static void RegisterHandlers(HttpConfiguration config)
        {
          //handlers here

        }

        public static void CreateRoutes(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(RpcApiRouteName, "api/{controller}/{action}/{id}", new
            {
                action = RouteParameter.Optional,
                id = RouteParameter.Optional
            });

            config.Routes.MapHttpRoute(DefaultApiRouteName, "api/{controller}/{id}", new
            {
                id = RouteParameter.Optional
            });
        }

        private static void RegisterTypes()
        {
         //IoC here
        }

        private static void RegisterMappings()
        {
           //mappings here
        }
    }
}
